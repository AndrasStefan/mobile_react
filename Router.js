import React from "react";
import { Scene, Stack, Router, Actions } from "react-native-router-flux";
import { StyleSheet, StatusBar } from "react-native";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Add from "./components/Add";
import Chart from "./components/Chart";

const RouterComponent = () => {
  return (
    <Router>
      <Stack hideNavBar key="root">
        <Stack key="auth" type="reset" titleStyle={style.titleStyle}>
          <Scene title="Sign In" key="login" component={Login} initial />
          <Scene title="Register" key="register" component={Register} />
        </Stack>
        <Stack key="main" type="reset" titleStyle={style.titleStyle}>
          <Scene title="Home" key="home" component={Home} initial />
          <Scene title="Add" key="add123" component={Add} />
          <Scene title="Chart" key="chart" component={Chart} />
        </Stack>
      </Stack>
    </Router>
  );
};

export default RouterComponent;

const style = StyleSheet.create({
  navBarStyle: {
    top: 100
  },
  titleStyle: {
    flexDirection: "row",
    width: 200
  }
});
