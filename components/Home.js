import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  List,
  ListItem,
  Icon,
  Right,
  Body
} from "native-base";
import axios from "axios";
import { Font, AppLoading } from "expo";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, todos: [] };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  handleRequest() {
    // This request will only succeed if the Authorization header
    // contains the API token
    axios
      .get("/auth/logout/")
      .then(response => {
        Actions.auth();
      })
      .catch(error => console.log(error.response));
  }

  handleDeleteRequest(item) {
    var pk = item.pk;
    console.log("In delete: " + pk);
    axios
      .delete(`/todo/${pk}/`)
      .then(response => {
        console.log("DELETED");
        this.getData();
      })
      .catch(error => console.log(error));
  }

  getData() {
    axios.get("/todos").then(response => {
      this.setState({ todos: response.data });
    });
  }

  componentDidMount() {
   this.getData(); 
  }

  render() {
    if (this.state.loading) {
      return <Text>loading</Text>;
    }

    var todos = [...this.state.todos];
    todos.sort((a, b) => ("" + a.priority).localeCompare(b.priority));

    // console.log(this.state.todos);
    // console.log(todos);

    return (
      <Container>
        <Content>
          <List
            dataArray={todos}
            renderRow={item => (
              <ListItem icon>
                <Body>
                  <Text>
                    {item.description} - {item.priority}
                  </Text>
                </Body>

                <Right>
                  <Button
                    style={{
                      backgroundColor: "#fa213b",
                      marginTop: 10,
                      marginBottom: 10
                    }}
                    onPress={this.handleDeleteRequest.bind(this, item)}
                  >
                    <Icon name="trash" />
                  </Button>
                </Right>
              </ListItem>
            )}
          />
        </Content>

        <Footer>
          <FooterTab>
            <Button full onPress={this.handleRequest.bind(this)}>
              <Text>Logout</Text>
            </Button>
            <Button full onPress={() => Actions.add123()}>
              <Text>Add</Text>
            </Button>
            <Button
              onPress={() => {
                Actions.chart();
              }}
            >
              <Image
                style={{ width: 66, height: 58 }}
                source={require("../assets/icon_chart.png")}
              />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainerStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "white"
  }
});

export default Home;
