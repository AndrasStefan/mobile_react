import React from "react";
import { Text } from "react-native-svg";
import { BarChart, Grid } from "react-native-svg-charts";

class Chart extends React.PureComponent {
  render() {
    const fill = "rgb(134, 65, 244)";
    const data = [10, 15, 8];

    return (
      <BarChart
        style={{ flex: 1 }}
        data={data}
        svg={{ fill }}
        contentInset={{ top: 20, bottom: 20, right: 10, left: 10 }}
      >
      <Grid/>
      </BarChart>
    );
  }
}

export default Chart;
