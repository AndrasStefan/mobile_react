import React, { Component } from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Text,
  Picker
} from "native-base";
import axios from "axios";
import { Actions } from "react-native-router-flux";

class Add extends Component {
  state = {
    description: "",
    priority: ""
  };

  onDescriptionChange(text) {
    // console.log(text);
    this.setState({ description: text });
  }

  onPriorityChange(text) {
    // console.log(text);
    this.setState({ priority: text });
  }

  handleRequest() {
    const payload = {
      description: this.state.description,
      priority: this.state.priority
    };

    // console.log(payload);

    axios
      .post("/todos/", payload)
      .then(response => {
        // console.log(response.data);
        // Navigate to the home screen
        Actions.main();
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Container>
        <Content contentContainerStyle={{ justifyContent: "center", flex: 1 }}>
          <Form>
            <Item>
              <Input
                placeholder="Description"
                onChangeText={this.onDescriptionChange.bind(this)}
              />
            </Item>
            <Item picker last>
              <Picker
                mode="dropdown"
                style={{ width: undefined }}
                placeholder="Select priority"
                placeholderStyle={{ color: "#bfc6ea" }}
                selectedValue={this.state.priority}
                onValueChange={this.onPriorityChange.bind(this)}
              >
                <Picker.Item label="High" value="high" />
                <Picker.Item label="Medium" value="medium" />
                <Picker.Item label="Low" value="low" />
              </Picker>
            </Item>
          </Form>
          <Button full success onPress={this.handleRequest.bind(this)}>
            <Text>Save</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Add;
