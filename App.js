import React, { Component } from "react";
import Router from "./Router";
import axios from "axios";
import { baseURL } from "./config";

export default class App extends Component {
  componentWillMount() {
    axios.defaults.baseURL = baseURL;
    axios.defaults.timeout = 1500;
  }

  render() {
    return <Router />;
  }
}
